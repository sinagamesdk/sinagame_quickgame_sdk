#新浪游戏游戏机SDK接入文档 
###1.更新历史
####2015-11-16 v1.0.5
```  
1，优化悬浮窗PopWindow加载左右布局文件的方式
2，修改切换横竖屏方向时错误重置高宽的bug
3，修正某些手机浮层崩溃的问题
4，修改获取屏幕尺寸的代码，兼容含虚拟按键的设备
```
###2.介绍
```  
1，无需安装、点开即玩，包含高性能游戏体系 
2，黑盒封装游戏机，接入方只需关心具体游戏
3，接入简单方便，配置好基础信息，调用一句函数即可完成所有接入工作
4，稳定、可靠，微博客户端已上线并商业化运作
5，接入过程中需测试查看后台收入情况，请找我方运营或商务同事申请账号 
```

###3.前言  
```  
接入之前，请确认您已跟我方运营同事获得了接入渠道号channel，如还未获取，请联系我方运营同事。  
```  

###4. 准备工作  
* 导入SDK包  
将SDK下的jar包部分拷贝到工程libs目录，将res资源文件部分拷贝到对应的目录（文件名请勿改动）
  

* 配置AndroidManifest  
在工程AndroidManifest.xml中配置用户权限
请将下面权限配置代码复制到 AndroidManifest.xml 文件中 :
```
       <!-- 点开即玩复制以下模块 -->
        <activity 
            android:name="com.sina.weibo.appmarket.sng.activity.SngGameDoorActivity"
            android:configChanges="keyboardHidden|orientation|screenSize"
            android:exported="true"
            android:theme="@style/sng_fullscreen_no_animstyle" >
        </activity>
         <!-- h5 game luncher -->
        <activity
            android:name="com.sina.weibo.appmarket.sng.activity.SngHtml5GameContainerActivity"
            android:configChanges="keyboardHidden|orientation|screenSize"
            android:exported="true"
            android:theme="@style/sng_fullscreen_no_animstyle" >
        </activity>
        
        <!-- egret runtime -->
        <activity
            android:name="com.sina.weibo.appmarket.sng.activity.SngEgretGameActivity"
            android:configChanges="keyboardHidden|orientation|screenSize"
            android:exported="true"
            android:theme="@style/sng_fullscreen_no_animstyle" >
        </activity>
        
        <!-- laya runtime -->
          <activity
            android:name="com.sina.weibo.appmarket.sng.activity.SngLayaGameActivity"
            android:configChanges="keyboardHidden|orientation|screenSize"
            android:exported="true"
            android:theme="@style/sng_fullscreen_no_animstyle" >
         </activity>
          
         <!-- 支付模块 -->
        <activity
            android:name="com.alipay.sdk.auth.AuthActivity"
            android:configChanges="orientation|keyboardHidden|navigation"
            android:exported="false"
            android:screenOrientation="behind"
            android:theme="@android:style/Theme.Light.NoTitleBar" >
        </activity>
        <activity
            android:name="com.alipay.sdk.app.H5PayActivity"
            android:configChanges="orientation|keyboardHidden|navigation"
            android:exported="false"
            android:screenOrientation="portrait"
            android:theme="@android:style/Theme.Light.NoTitleBar" >
        </activity>
        
        <!-- 客服模块 -->
         <activity 
            android:name="com.weibo.game.quick.sdk.feedback.FeedBackActivity"
            android:configChanges="keyboardHidden|orientation|screenSize"
            android:exported="true"
            android:theme="@style/sng_fullscreen_no_animstyle" >
        </activity>
        
         <activity 
            android:name="com.weibo.game.quick.sdk.feedback.ImageDetailActivity"
            android:configChanges="keyboardHidden|orientation|screenSize"
            android:exported="true"
            android:theme="@style/sng_fullscreen_no_animstyle" >
        </activity>
        
          <!-- 桌面启动方式入口 -->
        <activity
            android:name="com.sina.weibo.appmarket.sng.activity.SngGameLogicActivity"
            android:configChanges="keyboardHidden|orientation|screenSize"
            android:exported="true"
            android:theme="@style/sng_fullscreen_no_animstyle" >
        </activity>
        
        <meta-data
            android:name="sina_game_channel"
            android:value="sina_117110001000" > <!-- 修改这里规则为：sina_分配的channel -->
        </meta-data>
        <!-- 复制结束 -->
        
        
    </application>

    <!-- 基础权限 -->
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <!-- 快捷方式权限 -->
    <uses-permission android:name="com.android.launcher.permission.INSTALL_SHORTCUT" />
    <uses-permission android:name="com.android.launcher3.permission.INSTALL_SHORTCUT" />
    <uses-permission android:name="com.android.launcher3.permission.READ_SETTINGS" />
    <uses-permission android:name="com.android.launcher3.permission.WRITE_SETTINGS" />
    <uses-permission android:name="com.oppo.launcher.permission.READ_SETTINGS" />
    <uses-permission android:name="com.oppo.launcher.permission.WRITE_SETTINGS" />
    <uses-permission android:name="com.android.launcher.permission.READ_SETTINGS" />         
```

注：以下值需要替换（参数向我方运营同事获取），如下图：  
![分配渠道修改](http://git.oschina.net/uploads/images/2015/0917/202825_2c25100c_483144.png "分配渠道修改")


###5.在代码中使用
* 默认方式启动游戏

```
WeiboGameSDK.getInstance().openGame(activity,appkey,screenOrientation,oAuth,gameListener);
```


说明
```
activity：调用时当前页面
appkey：游戏分配的id
screenOrientation：横竖屏启动，1竖屏，2横屏
oAuth：interface实现实例,接入方授权成功后对接口中方法进行实现，供游戏方调用
gameListener：启动游戏的监听器

附接口定义:  
public interface GameListener {
    /**
     * 启动游戏
     * @param appkey 游戏启动key
     */
    public void onStart(String appkey);
    /**
     * 关闭游戏
     * @param appkey 游戏启动key
     * @param authInfo OAuth对象
     */
    public void onDestory(String appkey,OAuthInfo authInfo);
    
    
    /**
     * 启动失败，失败原因
     * @param appkey 游戏启动key
     * @param reason 失败原因，游戏机启动失败原因
     */
    public void onFailed(String appkey,String reason);
    
    /**
     * 登入失效
     * @param appkey 游戏启动key
     * @param authInfo OAuth对象
     */
    public void onLoginFailed(String appkey,OAuthInfo authInfo);
}


/**
 * 
 * 授权相关接口
 * 接入方实现这个接口，并组装成OAuthInfo对象
 * 
 */
public interface IOAuth {
        
    /**
     * 游戏启动器调用该方法获取用户信息
     * @return
     */
    public OAuthInfo getOAuth();
    
}
```

* SDK相关配置项

```
//设置调试模式传递true，**生产环境传递false**，默认设置true对应的Log日志TAG = "DEBUG"
WeiboGameSDK.debug(true);

//设置白鹭相关资源存储目录，建议存储在/data/data/包名/目录下，默认存储在/data/data/包名/files根目录下
WeiboGameSDK.setEgretDir(egretpath);

//设置白鹭相关资源存储目录，建议存储在/data/data/包名/目录下，默认存储在/data/data/包名/files根目录下
WeiboGameSDK.setLayaDir(layapath);

```





###6，打包测试
打包测试时，请确认server的接口对接已经完毕，启动的游戏是我方server提供的游戏数据源，另外检查【准备工作】中的配置是否已经完成，游戏分类型测试，例如h5系列游戏、runtime系列游戏 

###7，日志
Sdk使用“DEBUG”作为日志过滤标签，日志里可以看到sdk版本号，接口请求信息等。
![输入图片说明](http://git.oschina.net/uploads/images/2015/0813/171044_0cc7d1d1_483144.png "在这里输入图片标题")


###8.常见问题    