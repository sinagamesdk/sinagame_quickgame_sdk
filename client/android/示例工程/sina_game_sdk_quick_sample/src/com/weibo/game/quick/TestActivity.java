package com.weibo.game.quick;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.weibo.game.quick.sdk.OAuthInfo;
import com.weibo.game.quick.sdk.WeiboGameSDK;
import com.weibo.game.quick.sdk.interfaces.GameListener;
import com.weibo.game.quick.sdk.interfaces.IOAuth;

public class TestActivity extends Activity{
    //分别表示盗墓英雄，萌战机，愚公移山，萌猫闯三国，白鹭runtime恶魔大人,少女战机
    String[] appkeys = {"886504901","3546323715","4247282516","2820409645","892653483","1522428254"};
    //上面游戏对应的横竖屏：1竖屏，2横屏
    int[] screenOrientation = {1,1,1,2,2,1};
    
    /** TODO 三方需要实现的授权接口 */
    private IOAuth auth; 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button BTN = new Button(this);
        BTN.setText("测试");
        setContentView(BTN);
        auth = new IOAuth() {
            
            @Override
            public OAuthInfo getOAuth() {
                OAuthInfo info = new OAuthInfo();
                //测试万年token
                info.setToken("quickgame_sdk");
                return info;
            }
        };
        
        
        BTN.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                //测试游戏的appkey
                Random r = new Random();
                int index = r.nextInt(appkeys.length);
                WeiboGameSDK.debug(false);
                WeiboGameSDK.getInstance().openGame(TestActivity.this, appkeys[index],screenOrientation[index], auth, new GameListener() {
                    
                    @Override
                    public void onStart(String appkey) {
                        
                    }
                    
                    @Override
                    public void onLoginFailed(String appkey, OAuthInfo authInfo) {
                        
                    }
                    
                    @Override
                    public void onFailed(String appkey, String reason) {
                        
                    }
                    
                    @Override
                    public void onDestory(String appkey, OAuthInfo authInfo) {
                        
                    }
                });
            }
        });
        
    }
    
    
}
