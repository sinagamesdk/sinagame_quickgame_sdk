//
//  SNGQuickGameSDK.h
//  SNGQuickGameSDK
//
//  Created by xu huijun on 15/10/29.
//  Copyright © 2015年 SNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 * SNGQuickGameSDK接口类 
 */

typedef void(^onComplete)(BOOL onsuccess,NSString *reason,BOOL ondestory);

@interface SNGQuickGameSDK : NSObject

/**
 * 启动游戏
 * @param appkey            游戏启动key
 * @param token             第三方授权的身份标识
 * @param channelid         渠道号，例如：117110001000
 * @param usingUIWebView    是否使用原生UIWebView  使用：YES，不使用：NO
 * @param screenOrientation 1竖屏2横屏
 * @param viewController    打开所依托的UIViewController.
 * @param onComplete        onsuccess,YES：启动成功，NO:启动失败，reason：原因。ondestory：YES:主动关闭游戏，NO，自动关闭游戏（启动失败）

 */

+ (void)sng_quickGameStart:(NSString *)appkey
                     token:(NSString *)token
                 channelid:(NSString *)channelid
            usingUIWebView:(BOOL)usingUIWebView
         screenOrientation:(int)screenOrientation
        fromViewController:(UIViewController *)viewController
                onComplete:(onComplete)complete;

@end
