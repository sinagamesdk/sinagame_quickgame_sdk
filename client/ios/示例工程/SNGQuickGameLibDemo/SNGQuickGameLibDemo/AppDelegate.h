//
//  AppDelegate.h
//  SNGQuickGameLibDemo
//
//  Created by xu huijun on 15/10/29.
//  Copyright © 2015年 SNG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

