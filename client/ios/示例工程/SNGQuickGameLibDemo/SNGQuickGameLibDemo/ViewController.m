//
//  ViewController.m
//  SNGQuickGameLibDemo
//
//  Created by xu huijun on 15/10/29.
//  Copyright © 2015年 SNG. All rights reserved.
//

#import "ViewController.h"

#import "SNGQuickGameSDK.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - 
#pragma mark - quick_game_Action


-(IBAction)quickgameAction:(id)sender
{
  /**
   * 此处调用sdk点开即玩快速接口
   * appkey 萌战机:3546323715
   * token  @"quickgame_sdk"
   * channelid 渠道号:117110001000
   * WebView 不使用原生webview，使用WKWebView
   * screenOrientation 竖屏
   * fromViewController 依托
   */
    
    [SNGQuickGameSDK sng_quickGameStart:@"3546323715"
                                  token:@"quickgame_sdk"
                              channelid:@"117110001000"
                         usingUIWebView:NO
                      screenOrientation:1
                     fromViewController:self
                             onComplete:^(BOOL onsuccess, NSString *success, BOOL ondestory) {
                                 if (!onsuccess) {
                                     //游戏启动失败
                                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"标题"
                                                                                     message:success
                                                                                    delegate:nil
                                                                           cancelButtonTitle:@"确定"
                                                                           otherButtonTitles:nil];
                                     [alert show];
                                     
                                 }else if(onsuccess && ondestory){
                                     //关闭游戏
                                     
                                 }
                             }];
}


@end
