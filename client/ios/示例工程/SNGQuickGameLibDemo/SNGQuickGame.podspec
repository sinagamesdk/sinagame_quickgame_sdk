
Pod::Spec.new do |s|

s.name         = "SNGQuickGame"
s.version      = "0.0.1"
s.platform     = :ios, "6.0"
s.source       = { :git => "https://git.oschina.net/sinagamesdk/sinagame_quickgame_sdk.git", :tag => "0.0.1" }
s.source_files  = 'libSNGQuickGameSDK/*.{h,m}'
s.resource     = 'libSNGQuickGameSDK/SNGQuickGameNib.bundle','libSNGQuickGameSDK/SNGQuickGameResource.bundle'
s.frameworks   = 'SystemConfiguration', 'Security', 'MobileCoreServices'
s.vendored_libraries  = 'libSNGQuickGameSDK/libSNGQuickGameSDK.a'

s.requires_arc = true

end

