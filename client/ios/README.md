#新浪游戏点开即玩SDK接入文档 
###1.介绍
####2015-11-16 v1.0.0
```  
1，无需安装、点开即玩，包含高性能游戏体系 
2，黑盒封装游戏机，接入方只需关心具体游戏
3，接入简单方便，配置好基础信息，调用一句函数即可完成所有接入工作
4，稳定、可靠，微博客户端已上线并商业化运作
5，接入过程中需测试查看后台收入情况，请找我方运营或商务同事申请账号 
```

###2.前言  
```  
接入之前，请确认您已跟我方运营同事获得了接入渠道号channel，如还未获取，请联系我方运营同事。  
```  

###3. 准备工作  
* 导入SDK包  

```
  将libSNGQuickGameSDK文件夹添加至工程，其中包含：libSNGQuickGameSDK.a，SNGQuickGameNib.bundle，
  SNGQuickGameResource.bundle，SNGQuickGameSDK.h共4个文件。
```
* 添加依赖库
```
  添加framework：SystemConfiguration，Security，MobileCoreServices。
```
###4.在代码中使用 
```
* 启动方式

   /**
    * 启动游戏
    * @param appkey            游戏启动key
    * @param token             第三方授权的身份标识
    * @param channelid         渠道号，例如：10000010
    * @param usingUIWebView    是否使用原生UIWebView  使用：YES，不使用：NO
    * @param screenOrientation 1竖屏2横屏
    * @param viewController    打开所依托的UIViewController.
    * @param onComplete        onsuccess,YES：启动成功，NO:启动失败，reason：原因。
                               ondestory:YES:主动关闭游戏，NO，自动关闭游戏
    */

  + (void)sng_quickGameStart:(NSString *)appkey
                       token:(NSString *)token
                   channelid:(NSString *)channelid
              usingUIWebView:(BOOL)usingUIWebView
           screenOrientation:(int)screenOrientation
          fromViewController:(UIViewController *)viewController
                  onComplete:(onComplete)complete; 
```