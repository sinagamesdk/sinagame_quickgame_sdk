# 新浪游戏-游戏机SDK文档结构：

----------

 - [ 1. 服务端对接接口文档](https://git.oschina.net/sinagamesdk/sinagame_quickgame_sdk/blob/master/server/接口文档.md)
 - [ 2. Android端接入文档](https://git.oschina.net/sinagamesdk/sinagame_quickgame_sdk/blob/master/client/android/README.md)
 - [ 3. IOS接入文档](https://git.oschina.net/sinagamesdk/sinagame_quickgame_sdk/blob/master/client/ios/README.md)
 
